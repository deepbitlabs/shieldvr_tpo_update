﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphEventTrigger : MonoBehaviour {

	public Slider sliderTool;

	public GameManager gameManager;

    // Triggers
    bool Symmetry;
    bool Proportions;
    bool FacialShape;
    bool SkinQuality;
    bool Expression;

    // Message Dialogue for Graph
    public DialogueManager graphDialogue;
    ColliderController colliderController;

    public Renderer rend;

    // Triggered Events
    public int TriggerEvent;
    


    void Start()
    {
        colliderController = transform.GetComponentInParent<ColliderController>();

        if (colliderController == null) Debug.Log("Didn't find a ColliderController on parent gameObject");


        rend = GetComponentInParent<Renderer>();
        rend.enabled = false;

    }

   public  void HandleCollisionEntered()
   {
       colliderController.TurnOffAllCollidersExcept(this); // This disables all EXCEPT this gameObject
   }
    

    // Symmetry Section
    public void Asymmetry(float num)
	{
		sliderTool.value = 0.5f;
	}
}
