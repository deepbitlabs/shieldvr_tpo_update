﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinePointers : MonoBehaviour
{
    void Update()
    {
        Debug.DrawLine(Vector3.zero, new Vector3(1, 0, 0), Color.red);
    }

    void OnCollisionEnter(Collision col)
    {
        foreach (ContactPoint contact in col.contacts)
        {
            Debug.DrawLine(contact.point, contact.point + contact.normal, Color.green, 2, false);
        }
    }


}
