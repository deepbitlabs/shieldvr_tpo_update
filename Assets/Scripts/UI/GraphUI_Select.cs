﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphUI_Select : MonoBehaviour
{
    public GraphEventTrigger graphTrigger;

    public FA_Answer_UI FA_UI;

    public Graph_Arrow_UI graph_UI;


    // Showcase correct answers
    public GameObject reveal_answers;


    // Enabling Products


    void Start()
    {
        graph_UI.Symmetry_Arrow.enabled = true;

        reveal_answers.SetActive(false);
    }



    public void Symmetry0 ()
    {
        graphTrigger.HandleCollisionEntered();

        FA_UI.dialogue.text = FA_UI.dialogue0;

        graph_UI.Symmetry_Arrow.enabled = false;

        graph_UI.Proportion_Arrow.enabled = true;


        graphTrigger.TriggerEvent = 1;

    }

    public void Symmetry1()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue1;

        graph_UI.Symmetry_Arrow.enabled = false;

        graph_UI.Proportion_Arrow.enabled = true;

        graphTrigger.TriggerEvent = 1;
    }

    public void Symmetry2()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue2;

        graph_UI.Symmetry_Arrow.enabled = false;

        graph_UI.Proportion_Arrow.enabled = true;

        graphTrigger.TriggerEvent = 1;
    }

    public void Symmetry3()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue3;

        graph_UI.Symmetry_Arrow.enabled = false;

        graph_UI.Proportion_Arrow.enabled = true;

        graphTrigger.TriggerEvent = 1;
    }


    // Proportions
    public void Proportions0 ()
    {
        graphTrigger.HandleCollisionEntered();

        FA_UI.dialogue.text = FA_UI.dialogue0;

        graph_UI.Proportion_Arrow.enabled = false;
        graph_UI.FS_Arrow_VL.enabled = true;
    }

    public void Proportions1()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue1;

        graph_UI.Proportion_Arrow.enabled = false;
        graph_UI.FS_Arrow_VL.enabled = true;
    }

    public void Proportions2()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue2;

        graph_UI.Proportion_Arrow.enabled = false;
        graph_UI.FS_Arrow_VL.enabled = true;
    }

    public void Proportions3()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue3;

        graph_UI.Proportion_Arrow.enabled = false;
        graph_UI.FS_Arrow_VL.enabled = true;
    }


    // Facial Shape Volume Loss
    public void FS_VL0 ()
    {
        graphTrigger.HandleCollisionEntered();

        FA_UI.dialogue.text = FA_UI.dialogue0;

        graph_UI.FS_Arrow_VL.enabled = false;

        graph_UI.FS_Arrow_Sagging.enabled = true;
    }

    public void FS_VL1()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue1;

        graph_UI.FS_Arrow_VL.enabled = false;

        graph_UI.FS_Arrow_Sagging.enabled = true;
    }

    public void FS_VL2()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue2;

        graph_UI.FS_Arrow_VL.enabled = false;

        graph_UI.FS_Arrow_Sagging.enabled = true;
    }

    public void FS_VL3()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue3;

        graph_UI.FS_Arrow_VL.enabled = false;

        graph_UI.FS_Arrow_Sagging.enabled = true;
    }
    
    // Facial Shape Sagging
    public void FS_Sagging0 ()
    {
        graphTrigger.HandleCollisionEntered();

        FA_UI.dialogue.text = FA_UI.dialogue0;

        graph_UI.FS_Arrow_Sagging.enabled = false;
        graph_UI.SQ_Arrow_LOF.enabled = true;
    }

    public void FS_Sagging1()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue1;

        graph_UI.FS_Arrow_Sagging.enabled = false;
        graph_UI.SQ_Arrow_LOF.enabled = true;
    }

    public void FS_Sagging2()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue2;

        graph_UI.FS_Arrow_Sagging.enabled = false;
        graph_UI.SQ_Arrow_LOF.enabled = true;
    }

    public void FS_Sagging3()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue3;

        graph_UI.FS_Arrow_Sagging.enabled = false;
        graph_UI.SQ_Arrow_LOF.enabled = true;
    }
    
    
    
    
    
    /// <summary>
    /// Skin quality area
    /// </summary>
    
    // Facial Shape Sagging
    public void SQ_LOF0 ()
    {
        graphTrigger.HandleCollisionEntered();

        FA_UI.dialogue.text = FA_UI.dialogue0;

        graph_UI.SQ_Arrow_LOF.enabled = false;
        graph_UI.SQ_Arrow_LOR_GLOW.enabled = true;
    }

    public void SQ_LOF1()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue1;

        graph_UI.SQ_Arrow_LOF.enabled = false;
        graph_UI.SQ_Arrow_LOR_GLOW.enabled = true;
    }

    public void SQ_LOF2()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue2;

        graph_UI.SQ_Arrow_LOF.enabled = false;
        graph_UI.SQ_Arrow_LOR_GLOW.enabled = true;


    }

    public void SQ_LOF3()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue3;

        graph_UI.SQ_Arrow_LOF.enabled = false;
        graph_UI.SQ_Arrow_LOR_GLOW.enabled = true;
    } 
    
    // Facial Shape Sagging
    public void SQ_LOF_Glow0()
    {
        graphTrigger.HandleCollisionEntered();

        FA_UI.dialogue.text = FA_UI.dialogue0;

        graph_UI.SQ_Arrow_LOR_GLOW.enabled = false;

        graph_UI.Expression_Arrow_DL.enabled = true;
    }

    public void SQ_LOF_Glow1()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue1;

        graph_UI.SQ_Arrow_LOR_GLOW.enabled = false;

        graph_UI.Expression_Arrow_DL.enabled = true;
    }

    public void SQ_LOF_Glow2()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue2;

        graph_UI.SQ_Arrow_LOR_GLOW.enabled = false;

        graph_UI.Expression_Arrow_DL.enabled = true;
    }

    public void SQ_LOF_Glow3()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue3;

        graph_UI.SQ_Arrow_LOR_GLOW.enabled = false;

        graph_UI.Expression_Arrow_DL.enabled = true;
    }
    
    
    /// <summary>
    /// Expression Area
    /// </summary>
    
    // Facial Shape Sagging

    
    // Facial Shape Sagging
    public void E_DL0()
    {
        graphTrigger.HandleCollisionEntered();

        FA_UI.dialogue.text = FA_UI.dialogue0;

        graph_UI.Expression_Arrow_DL.enabled = false;
        graph_UI.Expression_Arrow_SL.enabled = true;
    }

    public void E_DL1()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue1;

        graph_UI.Expression_Arrow_DL.enabled = false;
        graph_UI.Expression_Arrow_SL.enabled = true;
    }

    public void E_DL2()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue2;

        graph_UI.Expression_Arrow_DL.enabled = false;
        graph_UI.Expression_Arrow_SL.enabled = true;
    }

    public void E_DL3()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue3;

        graph_UI.Expression_Arrow_DL.enabled = false;
        graph_UI.Expression_Arrow_SL.enabled = true;
    }



    public void E_SL0()
    {
        graphTrigger.HandleCollisionEntered();

        FA_UI.dialogue.text = FA_UI.dialogue0;

        
        graph_UI.Expression_Arrow_SL.enabled = false;


        // Graph Items
        //graphTrigger.Product1.SetActive(true);

        //graphTrigger.Product2.SetActive(true);

        //graphTrigger.dysportniddle.SetActive(true);
        //graphTrigger.restylaneniddle.SetActive(true);



        

        //graphTrigger.graphDialogue.dialogue.text = graphTrigger.graphDialogue.dialogue15;
        graphTrigger.graphDialogue.BeginTreatmentPlan();

        //Enabled Answers
        reveal_answers.SetActive(true);
    }

    public void E_SL1()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue1;

        graph_UI.Expression_Arrow_SL.enabled = false;

        // Graph Items
        //graphTrigger.Product1.SetActive(true);

        //graphTrigger.Product2.SetActive(true);

        //graphTrigger.dysportniddle.SetActive(true);
        //graphTrigger.restylaneniddle.SetActive(true);

        

        //graphTrigger.graphDialogue.dialogue.text = graphTrigger.graphDialogue.dialogue15;
        graphTrigger.graphDialogue.BeginTreatmentPlan();


        //Enabled Answers
        reveal_answers.SetActive(true);

    }

    public void E_SL2()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue2;

        graph_UI.Expression_Arrow_SL.enabled = false;

        // Graph Items
        //graphTrigger.Product1.SetActive(true);

        //graphTrigger.Product2.SetActive(true);

        //graphTrigger.dysportniddle.SetActive(true);
        //graphTrigger.restylaneniddle.SetActive(true);

        

        //graphTrigger.graphDialogue.dialogue.text = graphTrigger.graphDialogue.dialogue15;
        graphTrigger.graphDialogue.BeginTreatmentPlan();


        //Enabled Answers
        reveal_answers.SetActive(true);
    }

    public void E_SL3()
    {
        graphTrigger.HandleCollisionEntered();
        FA_UI.dialogue.text = FA_UI.dialogue3;

        graph_UI.Expression_Arrow_SL.enabled = false;

        // Graph Items
        //graphTrigger.Product1.SetActive(true);

        //graphTrigger.Product2.SetActive(true);

        //graphTrigger.dysportniddle.SetActive(true);
        //graphTrigger.restylaneniddle.SetActive(true);

        

        //graphTrigger.graphDialogue.dialogue.text = graphTrigger.graphDialogue.dialogue15;
        graphTrigger.graphDialogue.BeginTreatmentPlan();


        //Enabled Answers
        reveal_answers.SetActive(true);
    }

}
