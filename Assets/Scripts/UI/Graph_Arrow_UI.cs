﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Graph_Arrow_UI : MonoBehaviour
{
    public Image Symmetry_Arrow;
    public Image Proportion_Arrow;
    public Image FS_Arrow_VL;
    public Image FS_Arrow_Sagging;
    public Image SQ_Arrow_LOF;
    public Image SQ_Arrow_LOR_GLOW;
    public Image Expression_Arrow_DL;
    public Image Expression_Arrow_SL;

    void Start()
    {
        Symmetry_Arrow.enabled = false;
        Proportion_Arrow.enabled = false;
        FS_Arrow_VL.enabled = false;
        FS_Arrow_Sagging.enabled = false;


        SQ_Arrow_LOF.enabled = false;
        SQ_Arrow_LOR_GLOW.enabled = false;
        Expression_Arrow_DL.enabled = false;
        Expression_Arrow_SL.enabled = false;
    }

    public void Arrow_Direction_Symmetry()
    {
        Symmetry_Arrow.enabled = true;
    }

    public void Arrow_Direction_Proportion()
    {
        Proportion_Arrow.enabled = true;
    }

    public void Arrow_Direction_FS_VL()
    {
        FS_Arrow_VL.enabled = true;
    }

    public void Arrow_Direction_FS_Sagging()
    {
        FS_Arrow_Sagging.enabled = true;
    }

    public void Arrow_Direction_SQ_LOF()
    {
        SQ_Arrow_LOF.enabled = true;
    }

    public void Arrow_Direction_SQ_LOR_GLOW()
    {
        SQ_Arrow_LOR_GLOW.enabled = true;
    }

    public void Arrow_Direction_Expression_DL()
    {
        Expression_Arrow_DL.enabled = true;
    }

    public void Arrow_Direction_Expression_SL()
    {
        Expression_Arrow_SL.enabled = true;
    }

}
