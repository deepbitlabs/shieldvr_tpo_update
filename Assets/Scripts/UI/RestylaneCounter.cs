﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestylaneCounter : MonoBehaviour {

    public static int counter;

    public Text text;

    void Awake()
    {
        text = GetComponent<Text>();

        counter = 0;
    }

    void Update()
    {
        text.text = "" + counter;
    }

    public void Reset()
    {
        counter = 0;
    }
}
