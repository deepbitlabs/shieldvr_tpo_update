﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InjectionCounter : MonoBehaviour {

    public List<FacePoint> activeFacePoints = new List<FacePoint>();
    public int maxNumberOfFacePoints;


    public static int counter;

    public Text text;

    void Awake()
    {
        text = GetComponent<Text>();

        counter = 0;
    }

    void Update()
    {
        text.text = "" + activeFacePoints.Count;
    }

    public void Reset()
    {
        counter = 0;
    }
}
