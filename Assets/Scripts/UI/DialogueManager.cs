﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    /// <summary>
    /// This script will be called from other scripts
    /// </summary>
    public GameManager gameManager;

    [SerializeField]
    public Text dialogue = null;

    public string dialogue_intro = "This is Christine. She is 40 years old";
    public string dialogue1 = "In this experience you will assess the model’s face and then administer a treatment plan utilizing Galderma products.";
    public string dialogue2 = "Let's first start with assessing and grading the subject's face";
    public string dialogue3 = "Look at the subjects face and use the graph and legend to your right to grade the subject’s face.";
    public string dialogue4 = "Use your right controller to plot the points on the graph and use the trigger button to select your answers.";
    public string dialogue5 = "Now let's start with Symmetry. Look at the legend and mark the proper values on the graph based on the subject's face. Follow the arrow to complete the assessment.";
    public string dialogue6 = "Great job! Look to your right to see your answers and the correct answers of the facial assessment.";

    // Choosing other graph points
    public string dialogue7 = "Ok, Now choose a grade for Proportions";
    public string dialogue8 = "Ok, Now choose a grade for Facial Shape";
    public string dialogue9 = "Ok, Now choose a grade for Skin Quality";
    public string dialogue10 = "Ok, Now choose a grade for Expression";



    // Interacting with Dysport
    public string dialogue11 = "Now use your syringe to inject the subject in the proper areas for this product. HINT - There are 11 potential injections points. You must complete all of them to proceed";

    // Interacting with Rystylane
    public string dialogue12 = "Now use your syringe to inject the subject in proper areas for this product. HINT - There are 2 potential injections points. You must complete all of them to proceed";

    // Success Message Queue
    public string dialogue13 = "Great job! Here are all of the proper injection points for this product";

    // Failure Message Queue
    public string dialogue14 = "Here are the correct answers for the facial assessment";


    // Selecting a product
    public string dialogue15 = "Now that you've gone through the assessment, let's administer a treatment plan";

    // Simulation Completed!
    public string dialogue16 = "Thank you! You have completed the NSH SHIELD Virtual Facial Assessment & Injection Simulator";

    public string dialogue17 = "Look to your right to see your answers and the correct answers of the facial assessment.";

    public string dialogue18 = "Lets start with Dysport. Look to your left and grab the Dysport syringe using the trigger button";

    public string dialogue19 = "Look at the Treatment Plan Options Grid to your left and use your trigger to select each option to apply the proper treatment plan.";

    public string dialogue20 = "Starting with Column 1, select an area of the Face, then select a Product from column 2 and then in column 3 select an injection placement";

    public string dialogue21 = "Follow the arrows and complete the treatment plan.";

    public string dialogue22 = "Nice job! You have completed the Treatment Plan portion of this experience.";

    public string dialogue23 = "If you would like to try some more treatment plans click Restart. If not click End Session";

    public string dialogue24 = "Wrong combination... Please try again!";

    public string dialogue25 = "Please select a different Product";

    public string dialogue26 = "Please select a different Placement";

    public string dialogue27 = "Great Job!";

    public string dialogue28 = "Follow the arrows and complete the treatment plan.";



    void Start()
    {
        dialogue.text = dialogue_intro;

        

        StartCoroutine(InteractionText());
    }

    IEnumerator InteractionText()
    {
        yield return new WaitForSeconds(5);

        dialogue.text = dialogue1;

        yield return new WaitForSeconds(5);

        dialogue.text = dialogue2;

        yield return new WaitForSeconds(5);

        dialogue.text = dialogue3;

        yield return new WaitForSeconds(5);

        dialogue.text = dialogue4;

        yield return new WaitForSeconds(5);

        dialogue.text = dialogue5;

        gameManager.graphDisplay.SetActive(true);
    }


    // Dialogue for starting new treatment plan
    public void BeginTreatmentPlan()
    {
        StartCoroutine(TreatmentPlan_Start());
    }

    IEnumerator TreatmentPlan_Start()
    {
        yield return new WaitForSeconds(1);

        dialogue.text = dialogue17;

        yield return new WaitForSeconds(2);

        dialogue.text = dialogue15;

        yield return new WaitForSeconds(4);

        dialogue.text = dialogue19;

        yield return new WaitForSeconds(4);

        dialogue.text = dialogue20;

        yield return new WaitForSeconds(4);

        dialogue.text = dialogue21;

    }

    // Dialogue for selecting correct IP area
    public void Correct_IP_Selection()
    {
        StartCoroutine(IP_Correct());
    }

    IEnumerator IP_Correct()
    {
        yield return new WaitForSeconds(1);

        dialogue.text = dialogue27;

        yield return new WaitForSeconds(1);

        EndTreatmentPlan();
    }


    // Dialogue for ending new treatment plan
    public void EndTreatmentPlan()
    {
        StartCoroutine(TreatmentPlan_End());
    }

    IEnumerator TreatmentPlan_End()
    {
        yield return new WaitForSeconds(1);

        dialogue.text = dialogue22;

        yield return new WaitForSeconds(4);

        dialogue.text = dialogue23;
    }
}
