﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangingText : MonoBehaviour
{
    int index;

    string[] texts = new string[]
    {
        "This is Christine. She is 40 years old",
        "In this experience you will assess the model's face and then administer a treatment plan utilizing 2 Galderma products",
        "Let's first start with assessing and grading the subject's face",
        "Look at the subjects face and use the graph and legend to your right to assess the subject's face",
        "Use your right controller to plot the points on the graph, using the trigger button to select the value and the big circle button to choose your answer.",
        "Now let's start with Symmetry. Look at the legend and mark the proper value on the spider graph bases on the subject's",
        "Great! Now let's complete the assessment by choosing the proper value for each category within the graph",

        "Ok, Now choose a grade for Proportions",
        "Ok, Now choose a grade for Facial Shape",
        "Ok, Now choose a grade for Skin Quality",
        "Ok, Now choose a grade for Expression",

        "Now use your syringe to inject the subject in the proper areas for this product. HINT - There are 11 potential injections points. You must complete all of them to proceed",
        "Now use your syringe to inject the subject in proper areas for this product. HINT - There are 2 potential injections points. You must complete all of them to proceed",

        "Great job! Here are all of the proper injection points for this product",
        "Here are the correct answers for the facial assessment",

        "Now that you've gone through the assessment, now let's administer a treatment plan using Dysport and Restylane Lyft"
        //"Thank you! You have completed the NSH SHIELD Virtual Facial Assessment & Injection Simulator"


    };

    Text word;

    void Start()
    {
        int index = 0;
        word = GameObject.Find("DialogueBox").GetComponent<Text>();
        word.text = texts[index];
    }

    public void next()
    {
        if (index!= texts.Length)
        {
            index++;
            word.text = texts[index];
            print(texts[index]);
        }

        if (index==texts.Length)
        {
            index = texts.Length;
        }

    }

    public void previous()
    {
        if(index > 0)
        {
            index--;
            word.text = texts[index];
        }
        if (index!= texts.Length)
        {
            //index--;
            index = texts.Length;
        }

    }
}
