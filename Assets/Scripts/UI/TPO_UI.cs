﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TPO_UI : MonoBehaviour
{
    public GameObject GL_Dysport;
    public GameObject LCL_Dysport;
    public GameObject PF_Restylane;
    public GameObject Midface;
    public GameObject Lips_UI;

    //DialogueManager dialogue;

    void Start()
    {
        GL_Dysport.SetActive(false);
        LCL_Dysport.SetActive(false);
        PF_Restylane.SetActive(false);
        Midface.SetActive(false);
        Lips_UI.SetActive(false);

       // dialogue = GetComponent<DialogueManager>();
    }

    public void Enable_GL_Dysport_UI()
    {
        GL_Dysport.SetActive(true);
    }

    public void Enable_LCL_Dysport_UI()
    {
        LCL_Dysport.SetActive(true);
    }

    public void Enable_PF_Restylane_UI()
    {
        PF_Restylane.SetActive(true);
        //dialogue.EndTreatmentPlan();
    }

    public void Enable_Midface_UI()
    {
        Midface.SetActive(true);
    }

    public void Enable_Lips_UI()
    {
        Lips_UI.SetActive(true);
    }
}
