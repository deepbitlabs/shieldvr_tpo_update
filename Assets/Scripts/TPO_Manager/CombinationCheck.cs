﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombinationCheck : MonoBehaviour
{
    /// <summary>
    /// This section is the correct combination
    /// </summary>
    public List<GameObject> combo1;
    public List<GameObject> combo2;
    public List<GameObject> combo3;
    public List<GameObject> combo4;
    public List<GameObject> combo5;
    public List<GameObject> combo6;
    public List<GameObject> combo7;
    public List<GameObject> combo8;
    List<List<GameObject>> combinations;


    /// <summary>
    /// This section is the correct product selection
    /// </summary>
    ///
    public List<GameObject> correctproduct1;
    public List<GameObject> correctproduct2;
    public List<GameObject> correctproduct3;
    public List<GameObject> correctproduct4;
    public List<GameObject> correctproduct5;
    public List<GameObject> correctproduct6;
    public List<GameObject> correctproduct7;
    List<List<GameObject>> correct_product_selection;

    /// <summary>
    /// This section is the wrong product selection
    /// </summary>
    ///
    public List<GameObject> wrongproduct1;
    public List<GameObject> wrongproduct2;
    public List<GameObject> wrongproduct3;
    public List<GameObject> wrongproduct4;
    public List<GameObject> wrongproduct5;
    public List<GameObject> wrongproduct6;
    public List<GameObject> wrongproduct7;
    public List<GameObject> wrongproduct8;
    public List<GameObject> wrongproduct9;
    public List<GameObject> wrongproduct10;
    public List<GameObject> wrongproduct11;
    public List<GameObject> wrongproduct12;
    public List<GameObject> wrongproduct13;
    public List<GameObject> wrongproduct14;
    public List<GameObject> wrongproduct15;
    public List<GameObject> wrongproduct16;
    public List<GameObject> wrongproduct17;
    public List<GameObject> wrongproduct18;
    List<List<GameObject>> wrong_product_selection;


    // Checking for Injection placement
    public List<GameObject> wrongIPCombo1;
    public List<GameObject> wrongIPCombo2;
    public List<GameObject> wrongIPCombo3;
    public List<GameObject> wrongIPCombo4;
    public List<GameObject> wrongIPCombo5;
    public List<GameObject> wrongIPCombo6;
    public List<GameObject> wrongIPCombo7;
    public List<GameObject> wrongIPCombo8;
    public List<GameObject> wrongIPCombo9;
    public List<GameObject> wrongIPCombo10;
    public List<GameObject> wrongIPCombo11;
    public List<GameObject> wrongIPCombo12;
    public List<GameObject> wrongIPCombo13;
    public List<GameObject> wrongIPCombo14;
    public List<GameObject> wrongIPCombo15;
    public List<GameObject> wrongIPCombo16;
    public List<GameObject> wrongIPCombo17;
    public List<GameObject> wrongIPCombo18;
    public List<GameObject> wrongIPCombo19;
    public List<GameObject> wrongIPCombo20;
    public List<GameObject> wrongIPCombo21;
    public List<GameObject> wrongIPCombo22;
    public List<GameObject> wrongIPCombo23;
    public List<GameObject> wrongIPCombo24;
    public List<GameObject> wrongIPCombo25;
    public List<GameObject> wrongIPCombo26;
    public List<GameObject> wrongIPCombo27;
    List<List<GameObject>> wrong_IP_Combo_Selection;


    public List<GameObject> currentlyActive;

    public DialogueManager dialogue;

    // Session Buttons
    public GameObject SessionButton;

    // List of button images
    TPO_Event_Trigger tpo_eventTrigger;



    public Button[] tpo_event_trigger;

    public GameObject AOF_Arrow;
    public GameObject Product_Arrow;
    public GameObject IP_Arrow;


    



    void Start()
    {
        SessionButton.SetActive(false);

        //buttonImage = GetComponentsInChildren<Image>(true);

        tpo_eventTrigger = GetComponentInChildren<TPO_Event_Trigger>();


        AOF_Arrow.SetActive(true);
        Product_Arrow.SetActive(false);
        IP_Arrow.SetActive(false);

        //tpo_eventTrigger.tpo_rend.enabled = true;

        wrong_IP_Combo_Selection = new List<List<GameObject>>()
        {
            wrongIPCombo1,
            wrongIPCombo2,
            wrongIPCombo3,
            wrongIPCombo4,
            wrongIPCombo5,
            wrongIPCombo6,
            wrongIPCombo7,
            wrongIPCombo8,
            wrongIPCombo9,
            wrongIPCombo10,
            wrongIPCombo11,
            wrongIPCombo12,
            wrongIPCombo13,
            wrongIPCombo14,
            wrongIPCombo15,
            wrongIPCombo16,
            wrongIPCombo17,
            wrongIPCombo18,
            wrongIPCombo19,
            wrongIPCombo20,
            wrongIPCombo21,
            wrongIPCombo22,
            wrongIPCombo23,
            wrongIPCombo24,
            wrongIPCombo25,
            wrongIPCombo26,
            wrongIPCombo27
        };

        combinations = new List<List<GameObject>>()
        {
            combo1,
            combo2,
            combo3,
            combo4,
            combo5,
            combo6,
            combo7,
            combo8
        };

        wrong_product_selection = new List<List<GameObject>>()
        {
            wrongproduct1,
            wrongproduct2,
            wrongproduct3,
            wrongproduct4,
            wrongproduct5,
            wrongproduct6,
            wrongproduct7,
            wrongproduct8,
            wrongproduct9,
            wrongproduct10,
            wrongproduct11,
            wrongproduct12,
            wrongproduct13,
            wrongproduct14,
            wrongproduct15,
            wrongproduct16,
            wrongproduct17,
            wrongproduct18

        };


        correct_product_selection = new List<List<GameObject>>()
        {
            correctproduct1,
            correctproduct2,
            correctproduct3,
            correctproduct4,
            correctproduct5,
            correctproduct6,
            correctproduct7
        };



        // Check injection placement list

    }


    public void ResetSelection(GameObject myButton)
    {

        foreach (List<GameObject> combinations in combinations)
        {
            currentlyActive.Remove(myButton);

            Debug.Log("Removing gameObject from list");
        }
    }



    public void ButtonPressed(GameObject myButton)
    {
        if (!currentlyActive.Contains(myButton))
        {
            currentlyActive.Add(myButton);
        }
        else if (currentlyActive.Contains(myButton))
        {
            currentlyActive.Remove(myButton);
        }

        bool foundMatch = false;

        bool NoMatchfound = false;

        AOF_Arrow.SetActive(false);
        Product_Arrow.SetActive(true);
        IP_Arrow.SetActive(false);



        foreach (List<GameObject> combination in combinations)
        {
            int matches = 0;


            foreach (GameObject chosenButton in currentlyActive)
            {
                if (combination.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count == combination.Count && matches == combination.Count)
            {
                foundMatch = true;
                //dialogue.dialogue.text = dialogue.dialogue23;
                //CorrectCombinationMessage();
                Correct_IP_Selection();
                SessionButton.SetActive(true);
                //tpo_eventTrigger.colliderController.enabled = false;

                //currentlyActive.Remove(myButton);

                //comboManager.ResetEverything();


                //ResetSelection(myButton);

                AOF_Arrow.SetActive(false);
                Product_Arrow.SetActive(false);
                IP_Arrow.SetActive(false);
            }
        }


        foreach (List<GameObject> wrong_product_selection in wrong_product_selection)
        {
            int matches = 0;

            foreach (GameObject chosenButton in currentlyActive)
            {
                if (wrong_product_selection.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count == wrong_product_selection.Count && matches == wrong_product_selection.Count)
            {
                currentlyActive.Remove(myButton);
                Debug.LogError("Wrong Product Item Selection");
                dialogue.dialogue.text = dialogue.dialogue25;
                AOF_Arrow.SetActive(false);
                Product_Arrow.SetActive(true);
            }
        }

        foreach (List<GameObject> correct_product_selection in correct_product_selection)
        {
            int matches = 0;

            foreach (GameObject chosenButton in currentlyActive)
            {
                if (correct_product_selection.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count == correct_product_selection.Count && matches == correct_product_selection.Count)
            {
                //currentlyActive.Remove(myButton);
                Debug.LogError("Correct Product Item Selection");
                dialogue.dialogue.text = dialogue.dialogue27;
                Product_Arrow.SetActive(false);
                IP_Arrow.SetActive(true);
            }
        }

        foreach (List<GameObject> wrong_IP_Combo_Selection in wrong_IP_Combo_Selection)
        {
            int matches = 0;

            foreach (GameObject chosenButton in currentlyActive)
            {
                if (wrong_IP_Combo_Selection.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count == wrong_IP_Combo_Selection.Count && matches == wrong_IP_Combo_Selection.Count)
            {
                currentlyActive.Remove(myButton);
                Debug.LogError("Sorry, Please select a correct injection placement");
                dialogue.dialogue.text = dialogue.dialogue26;
                Product_Arrow.SetActive(false);
                IP_Arrow.SetActive(true);
            }
        }


    }




    void CorrectCombinationMessage()
    {
        StartCoroutine(SuccessMessage());
    }

    IEnumerator SuccessMessage()
    {
        yield return new WaitForSeconds(1);

        dialogue.dialogue.text = dialogue.dialogue22;

        yield return new WaitForSeconds(3);

        dialogue.dialogue.text = dialogue.dialogue23;
    }


    // This dialogue action starts when selecting the correct IP area
    public void Correct_IP_Selection()
    {
        StartCoroutine(IP_Correct());
    }

    IEnumerator IP_Correct()
    {
        yield return new WaitForSeconds(1);

        dialogue.dialogue.text = dialogue.dialogue27;

        yield return new WaitForSeconds(1);

        CorrectCombinationMessage();
    }


    public void ResetButtonSelection()
    {

        currentlyActive.Clear();
        AOF_Arrow.SetActive(true);
        Product_Arrow.SetActive(false);
        IP_Arrow.SetActive(false);
    }
}
