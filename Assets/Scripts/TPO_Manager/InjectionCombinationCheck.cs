﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InjectionCombinationCheck : MonoBehaviour
{
    /// <summary>
    /// This section is the correct IP combination
    /// </summary>
    public List<GameObject> correctIP1;
    public List<GameObject> correctIP2;
    public List<GameObject> correctIP3;
    public List<GameObject> correctIP4;
    public List<GameObject> correctIP5;
    public List<GameObject> correctIP6;
    public List<GameObject> correctIP7;
    public List<GameObject> correctIP8;
    List<List<GameObject>> correctIPCombo;


    /// <summary>
    /// This section is the wrong IP combination
    /// </summary>
    public List<GameObject> wrongIP1;
    public List<GameObject> wrongIP2;
    public List<GameObject> wrongIP3;
    public List<GameObject> wrongIP4;
    public List<GameObject> wrongIP5;
    public List<GameObject> wrongIP6;
    public List<GameObject> wrongIP7;
    public List<GameObject> wrongIP8;
    public List<GameObject> wrongIP9;
    public List<GameObject> wrongIP10;
    public List<GameObject> wrongIP11;
    public List<GameObject> wrongIP12;
    public List<GameObject> wrongIP13;
    public List<GameObject> wrongIP14;
    public List<GameObject> wrongIP15;
    public List<GameObject> wrongIP16;
    public List<GameObject> wrongIP17;
    public List<GameObject> wrongIP18;
    public List<GameObject> wrongIP19;
    public List<GameObject> wrongIP20;
    List<List<GameObject>> wrongIPCombo;


    public List<GameObject> currentlyActive;


    void Start()
    {
        correctIPCombo = new List<List<GameObject>>()
        {
            correctIP1,
            correctIP2,
            correctIP3,
            correctIP4,
            correctIP5,
            correctIP6,
            correctIP7,
            correctIP8
        };

        wrongIPCombo = new List<List<GameObject>>()
        {
            wrongIP1,
            wrongIP2,
            wrongIP3,
            wrongIP4,
            wrongIP5,
            wrongIP6,
            wrongIP7,
            wrongIP8,
            wrongIP9,
            wrongIP10,
            wrongIP11,
            wrongIP12,
            wrongIP13,
            wrongIP14,
            wrongIP15,
            wrongIP16,
            wrongIP17,
            wrongIP18,
            wrongIP19,
            wrongIP20
        };
    }

    public void IPButtonCheck(GameObject button)
    {
        if (currentlyActive.Contains(button))
        {
            currentlyActive.Add(button);
        }
        else if (currentlyActive.Contains(button))
        {
            currentlyActive.Remove(button);
        }

        foreach (List<GameObject> correctIPCombo in correctIPCombo)
        {
            int matches = 0;

            foreach (GameObject chosenButton in currentlyActive)
            {
                if (correctIPCombo.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count == correctIPCombo.Count && matches == correctIPCombo.Count)
            {
                Debug.LogError("Success! You select the correct IP area");
            }
        }

        foreach (List<GameObject> wrongIPCombo in wrongIPCombo)
        {
            int matches = 0;

            foreach (GameObject chosenButton in currentlyActive)
            {
                if (wrongIPCombo.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count == wrongIPCombo.Count && matches == wrongIPCombo.Count)
            {
                Debug.LogError("Sorry! You select the wrong IP area");
            }
        }
    }








}
