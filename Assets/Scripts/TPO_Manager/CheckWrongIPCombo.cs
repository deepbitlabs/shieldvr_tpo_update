﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CheckWrongIPCombo : MonoBehaviour
{
    public List<GameObject> ipcombo1;
    public List<GameObject> ipcombo2;
    public List<GameObject> ipcombo3;
    public List<GameObject> ipcombo4;
    public List<GameObject> ipcombo5;
    public List<GameObject> ipcombo6;
    public List<GameObject> ipcombo7;
    public List<GameObject> ipcombo8;
    public List<GameObject> ipcombo9;
    public List<GameObject> ipcombo10;
    public List<GameObject> ipcombo11;
    public List<GameObject> ipcombo12;
    public List<GameObject> ipcombo13;
    public List<GameObject> ipcombo14;
    public List<GameObject> ipcombo15;
    public List<GameObject> ipcombo16;
    public List<GameObject> ipcombo17;
    public List<GameObject> ipcombo18;
    public List<GameObject> ipcombo19;
    public List<GameObject> ipcombo20;
    public List<GameObject> ipcombo21;
    public List<GameObject> ipcombo22;
    public List<GameObject> ipcombo23;
    public List<GameObject> ipcombo24;
    public List<GameObject> ipcombo25;
    public List<GameObject> ipcombo26;
    public List<GameObject> ipcombo27;
    public List<GameObject> ipcombo28;
    List<List<GameObject>> ip_combinations;

    public List<GameObject> currentlyActive;

    // Reset Everything!
    public CombinationManager comboManager;


    public DialogueManager dialogue;

    void Start()
    {

        comboManager = GetComponent<CombinationManager>();

        ip_combinations = new List<List<GameObject>>()
        {
            ipcombo1,
            ipcombo2,
            ipcombo3,
            ipcombo4,
            ipcombo5,
            ipcombo6,
            ipcombo7,
            ipcombo8,
            ipcombo9,
            ipcombo10,
            ipcombo11,
            ipcombo12,
            ipcombo13,
            ipcombo14,
            ipcombo15,
            ipcombo16,
            ipcombo17,
            ipcombo18,
            ipcombo19,
            ipcombo20,
            ipcombo21,
            ipcombo22,
            ipcombo23,
            ipcombo24,
            ipcombo25,
            ipcombo26,
            ipcombo27,
            ipcombo28
        };
    }

    public void IP_Button(GameObject button)
    {
        if (!currentlyActive.Contains(button))
        {
            currentlyActive.Add(button);
        }
        else if (currentlyActive.Contains(button))
        {
            currentlyActive.Remove(button);
        }

        bool wrongproduct = false;


        foreach (List<GameObject> ip_combination in ip_combinations)
        {
            int matches = 0;

            foreach (GameObject chosenButton in currentlyActive)
            {
                if (ip_combination.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count == ip_combination.Count && matches == ip_combination.Count)
            {
                wrongproduct = true;
                Debug.Log("Wrong injection placement selected!");

                // Adding UI Dialogue
                dialogue.dialogue.text = dialogue.dialogue26;

                comboManager.ResetComboSelect(button);


                // Removing item from list
                currentlyActive.Remove(button);

                //comboManager.ResetEverything();

                //comboManager.ResetComboCheck(this);

            }
        }
    }

    public void ResetButtonSelect()
    {
        currentlyActive.Clear();

    }
}
