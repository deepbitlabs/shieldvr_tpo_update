﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Product
{
    public string name;
    public string[] injectplacements = new string[5];
    public Product(string aName)
    {
        name = aName;

        if (name == "d_product")
        {
            injectplacements[0] = "Muscle";

        }

        if (name == "rl_product")
        {
            injectplacements[0] = "dermis_ip";
            injectplacements[0] = "sc_ip";
            injectplacements[0] = "sp_ip";
        }

        if (name == "r_product")
        {
            injectplacements[0] = "dermis_ip";
        }

        if (name == "rs_product")
        {
            injectplacements[0] = "sm_ip";
        }

        if (name == "rd_product")
        {
            injectplacements[0] = "dermis_ip";
        }
    }
}
