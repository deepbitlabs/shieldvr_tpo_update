﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorrectProductCheck : MonoBehaviour
{
    public List<GameObject> productcheck1;
    public List<GameObject> productcheck2;
    public List<GameObject> productcheck3;
    public List<GameObject> productcheck4;
    public List<GameObject> productcheck5;
    public List<GameObject> productcheck6;
    public List<GameObject> productcheck7;
    List<List<GameObject>> correct_products;


    public List<GameObject> wrongproductcheck1;
    public List<GameObject> wrongproductcheck2;
    public List<GameObject> wrongproductcheck3;
    public List<GameObject> wrongproductcheck4;
    public List<GameObject> wrongproductcheck5;
    public List<GameObject> wrongproductcheck6;
    public List<GameObject> wrongproductcheck7;
    List<List<GameObject>> wrong_products;

    public List<GameObject> currentlyActive;


    // Application Dialogue
    public DialogueManager dialogue;


    // Manager
    public CombinationManager comboManager;


    void Start()
    {
        correct_products = new List<List<GameObject>>()
        {
            productcheck1,
            productcheck2,
            productcheck3,
            productcheck4,
            productcheck5,
            productcheck6,
            productcheck7
        };


        wrong_products = new List<List<GameObject>>()
        {
            wrongproductcheck1,
            wrongproductcheck2,
            wrongproductcheck3,
            wrongproductcheck4,
            wrongproductcheck5,
            wrongproductcheck6,
            wrongproductcheck7
        };
    }

    public void Reset_Product_Selection(GameObject productbutton)
    {
        

        foreach (List<GameObject> correct_products in correct_products)
        {
            currentlyActive.Remove(productbutton);
            Debug.LogError("Removing gameobject from list");
        }
    }

    public void correct_button_check(GameObject button)
    {
        if (!currentlyActive.Contains(button))
        {
            currentlyActive.Add(button);
        }
        else if (currentlyActive.Contains(button))
        {
            currentlyActive.Remove(button);
        }

        bool correctproduct = false;

        foreach (List<GameObject> correct_product in correct_products)
        {
            int matches = 0;

            foreach(GameObject chosenButton in currentlyActive)
            {
                if (correct_product.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count == correct_product.Count && matches == correct_product.Count)
            {
                correctproduct = true;
                Debug.Log("Correct product selected!");

                dialogue.dialogue.text = dialogue.dialogue27;

            }
        }

        foreach(List<GameObject> wrong_product in wrong_products)
        {
            int matches = 0;

            foreach(GameObject chosenButton in currentlyActive)
            {
                if (wrong_product.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count == wrong_product.Count && matches == wrong_product.Count)
            {
                Debug.LogError("Wrong product item selected!");
                currentlyActive.Remove(button);

            }
        }

    }
}
