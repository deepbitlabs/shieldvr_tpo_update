﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TPO_Arrow_UI : MonoBehaviour
{


    public GameObject AOF_Arrow;
    public GameObject Product_Arrow;
    public GameObject IP_Arrow;

    void Start()
    {
        // Leaving it blank for now!
    }



    public void Enable_AOF_Arrow()
    {
        AOF_Arrow.SetActive(true);
        Product_Arrow.SetActive(false);
        IP_Arrow.SetActive(false);
    }

    public void Disable_AOF_Arrow()
    {
        AOF_Arrow.SetActive(false);
        Product_Arrow.SetActive(false);
        IP_Arrow.SetActive(false);
    }

    public void Disable_Arrow()
    {
        AOF_Arrow.SetActive(false);
        Product_Arrow.SetActive(false);
        IP_Arrow.SetActive(false);
    }


    public void Enable_Product_Arrow()
    {
        AOF_Arrow.SetActive(false);
        Product_Arrow.SetActive(true);
        IP_Arrow.SetActive(false);
    }

    public void Disable_Product_Arrow()
    {
        AOF_Arrow.SetActive(false);
        Product_Arrow.SetActive(false);
        IP_Arrow.SetActive(false);
    }

    public void Enable_IP_Arrow()
    {
        AOF_Arrow.SetActive(false);
        Product_Arrow.SetActive(false);
        IP_Arrow.SetActive(true);
    }

    public void Disable_IP_Arrow()
    {
        AOF_Arrow.SetActive(false);
        Product_Arrow.SetActive(false);
        IP_Arrow.SetActive(false);
    }
}
