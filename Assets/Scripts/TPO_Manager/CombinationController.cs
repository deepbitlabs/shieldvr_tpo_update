﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombinationController : MonoBehaviour
{
    //// Checking correct combos
    //public List<GameObject> combo1;
    //public List<GameObject> combo2;
    //public List<GameObject> combo3;
    //public List<GameObject> combo4;
    //public List<GameObject> combo5;
    //public List<GameObject> combo6;
    //public List<GameObject> combo7;
    //public List<GameObject> combo8;
    //List<List<GameObject>> combinations;

    //public List<GameObject> currentlyActive_Combos;


    //// Checking for List Products
    //public List<GameObject> productcombo1;
    //public List<GameObject> productcombo2;
    //public List<GameObject> productcombo3;
    //public List<GameObject> productcombo4;
    //public List<GameObject> productcombo5;
    //public List<GameObject> productcombo6;
    //public List<GameObject> productcombo7;
    //public List<GameObject> productcombo8;
    //public List<GameObject> productcombo9;
    //public List<GameObject> productcombo10;
    //public List<GameObject> productcombo11;
    //public List<GameObject> productcombo12;
    //public List<GameObject> productcombo13;
    //public List<GameObject> productcombo14;
    //public List<GameObject> productcombo15;
    //public List<GameObject> productcombo16;
    //public List<GameObject> productcombo17;
    //public List<GameObject> productcombo18;
    //List<List<GameObject>> product_combinations;

    //public List<GameObject> currentlyActive_products;


    //// Checking for List of Injection Placements
    //public List<GameObject> ipcombo1;
    //public List<GameObject> ipcombo2;
    //public List<GameObject> ipcombo3;
    //public List<GameObject> ipcombo4;
    //public List<GameObject> ipcombo5;
    //public List<GameObject> ipcombo6;
    //public List<GameObject> ipcombo7;
    //public List<GameObject> ipcombo8;
    //public List<GameObject> ipcombo9;
    //public List<GameObject> ipcombo10;
    //public List<GameObject> ipcombo11;
    //public List<GameObject> ipcombo12;
    //public List<GameObject> ipcombo13;
    //public List<GameObject> ipcombo14;
    //public List<GameObject> ipcombo15;
    //public List<GameObject> ipcombo16;
    //public List<GameObject> ipcombo17;
    //public List<GameObject> ipcombo18;
    //public List<GameObject> ipcombo19;
    //public List<GameObject> ipcombo20;
    //public List<GameObject> ipcombo21;
    //public List<GameObject> ipcombo22;
    //public List<GameObject> ipcombo23;
    //public List<GameObject> ipcombo24;
    //public List<GameObject> ipcombo25;
    //public List<GameObject> ipcombo26;
    //public List<GameObject> ipcombo27;
    //public List<GameObject> ipcombo28;
    //List<List<GameObject>> ip_combinations;

    //public List<GameObject> currentlyActive_ip;


    //void Start()
    //{
    //    combinations = new List<List<GameObject>>()
    //    {
    //        combo1,
    //        combo2,
    //        combo3,
    //        combo4,
    //        combo5,
    //        combo6,
    //        combo7,
    //        combo8
    //    };


    //    product_combinations = new List<List<GameObject>>()
    //    {
    //        productcombo1,
    //        productcombo2,
    //        productcombo3,
    //        productcombo4,
    //        productcombo5,
    //        productcombo6,
    //        productcombo7,
    //        productcombo8,
    //        productcombo9,
    //        productcombo10,
    //        productcombo11,
    //        productcombo12,
    //        productcombo13,
    //        productcombo14,
    //        productcombo15,
    //        productcombo16,
    //        productcombo17,
    //        productcombo18
    //    };



    //    ip_combinations = new List<List<GameObject>>()
    //    {
    //        ipcombo1,
    //        ipcombo2,
    //        ipcombo3,
    //        ipcombo4,
    //        ipcombo5,
    //        ipcombo6,
    //        ipcombo7,
    //        ipcombo8,
    //        ipcombo9,
    //        ipcombo10,
    //        ipcombo11,
    //        ipcombo12,
    //        ipcombo13,
    //        ipcombo14,
    //        ipcombo15,
    //        ipcombo16,
    //        ipcombo17,
    //        ipcombo18,
    //        ipcombo19,
    //        ipcombo20,
    //        ipcombo21,
    //        ipcombo22,
    //        ipcombo23,
    //        ipcombo24,
    //        ipcombo25,
    //        ipcombo26,
    //        ipcombo27,
    //        ipcombo28
    //    };
    //}


    //public void ConfirmCombo(GameObject myButton)
    //{
    //    if (!currentlyActive_Combos.Contains(myButton))
    //    {
    //        currentlyActive_Combos.Add(myButton);
    //    }
    //    else if (currentlyActive_Combos.Contains(myButton))
    //    {
    //        currentlyActive_Combos.Remove(myButton);
    //    }


    //    foreach (List<GameObject> combination in combinations)
    //    {
    //        int matches = 0;

    //        foreach (GameObject chosenButton in currentlyActive_Combos)
    //        {
    //            if (combination.Contains(chosenButton))
    //            {
    //                matches++;
    //            }
    //        }


    //        if (currentlyActive_Combos.Count == combinations.Count && matches == combinations.Count)
    //        {
    //            // Combination is correct... Return success message
    //        }
    //    }
    //}


    //public void ProductSelect(GameObject product_button)
    //{
    //    if (!currentlyActive_products.Contains(product_button))
    //    {
    //        currentlyActive_products.Add(product_button);
    //    }
    //    else if (currentlyActive_products.Contains(product_button))
    //    {
    //        currentlyActive_products.Remove(product_button);
    //    }

    //    foreach (List<GameObject> product_combination in product_combinations)
    //    {
    //        int matches = 0;


    //        foreach (GameObject chosenButton in currentlyActive_products)
    //        {
    //            if (product_combinations.Contains(chosenButton))
    //            {
    //                matches++;
    //            }
    //        }

    //        if (currentlyActive_products.Count == product_combination.Count && matches == product_combination.Count)
    //        {
    //            // Return a error message here if user selects wrong product

    //        }
    //    }
    //}

    //public void IP_Select(GameObject ip_button)
    //{
    //    if (!currentlyActive_ip.Contains(ip_button))
    //    {
    //        currentlyActive_ip.Add(ip_button);
    //    }
    //    else if (currentlyActive_ip.Contains(ip_button))
    //    {
    //        currentlyActive_ip.Remove(ip_button);
    //    }


    //    //bool wrongIPSelection = false;

    //    foreach (List<GameObject> ip_combination in ip_combinations)
    //    {
    //        int matches = 0;

    //        foreach (GameObject chosenButton in currentlyActive_ip)
    //        {
    //            if (ip_combination.Contains(chosenButton))
    //            {
    //                matches++;
    //            }
    //        }

    //        if (currentlyActive_ip.Count == ip_combination.Count && matches == ip_combination.Count)
    //        {
    //            //bool wrongIPSelection = true;


    //            // Delete item selected from combination check list
    //            //currentlyActive_Combos.Remove(myButton);


    //            // Display error message related to this selection
    //        }
    //    }
    //}
}
