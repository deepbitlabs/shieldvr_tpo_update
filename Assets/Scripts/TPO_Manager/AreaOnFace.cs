﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaOnFace
{
    public string name;
    public Product[] products = new Product[5];
    public AreaOnFace(string aName)
    {
        name = aName;
    }
}
