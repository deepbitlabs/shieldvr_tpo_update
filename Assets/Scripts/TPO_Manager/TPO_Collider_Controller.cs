﻿using UnityEngine;
using UnityEngine.UI;

public class TPO_Collider_Controller : MonoBehaviour
{
    [SerializeField]
    TPO_Event_Trigger[] eventTrigger;


    void Start()
    {
        eventTrigger = transform.GetComponentsInChildren<TPO_Event_Trigger>();


    }

    public void TurnOffAllCollidersExcept(TPO_Event_Trigger tpo_events)
    {
        Debug.Log("Turned off all colliders except called by" + tpo_events.gameObject.name);

        foreach (TPO_Event_Trigger trigger in eventTrigger)
        {
            Image tpo_rend = trigger.GetComponentInChildren<Image>();

            if (trigger != tpo_events)
            {

                tpo_rend.enabled = false;
                Debug.Log("Disabled collider on: " + trigger.name);
            }
            else
            {
                tpo_rend.enabled = true;
                Debug.Log("Enabled collider on: " + trigger.name);
            }
        }
    }
}
