﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TPOUI_Select : MonoBehaviour
{
    public TPO_Event_Trigger tpo_eventTrigger;

    public Image tpo_image;

    void Start()
    {

        tpo_image = GetComponent<Image>();
        tpo_image.enabled = false;

        

    }


    // Area On Face
    public void GL_AOF()
    {
        tpo_eventTrigger.HandleCollisionEntered();


        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;
    }

    public void MF_AOF()
    {
        tpo_eventTrigger.HandleCollisionEntered();



        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;

    }

    public void NLF_AOF()
    {
        tpo_eventTrigger.HandleCollisionEntered();


        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;

    }

    public void Lips_AOF()
    {
        tpo_eventTrigger.HandleCollisionEntered();


        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;

    }

    public void PL_AOF()
    {
        tpo_eventTrigger.HandleCollisionEntered();


        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;

    }


    // Products
    public void D_Product()
    {
        tpo_eventTrigger.HandleCollisionEntered();

        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;

    }

    public void R_Product()
    {
        tpo_eventTrigger.HandleCollisionEntered();


        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;
    }

    public void RL_Product()
    {
        tpo_eventTrigger.HandleCollisionEntered();


        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;
    }

    public void RS_Product()
    {
        tpo_eventTrigger.HandleCollisionEntered();


        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;
    }

    public void RD_Product()
    {
        tpo_eventTrigger.HandleCollisionEntered();

        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;
    }


    // Injection Placement
    public void M_IP()
    {
        tpo_eventTrigger.HandleCollisionEntered();


        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;

    }


    public void SC_IP()
    {
        tpo_eventTrigger.HandleCollisionEntered();

        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;
    }

    public void D_IP()
    {
        tpo_eventTrigger.HandleCollisionEntered();

        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;
    }

    public void SM_IP()
    {
        tpo_eventTrigger.HandleCollisionEntered();

        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;
    }

    public void SP_IP()
    {
        tpo_eventTrigger.HandleCollisionEntered();

        tpo_image.enabled = true;

        //tpo_eventTrigger.tpo_rend.enabled = true;
    }
}
