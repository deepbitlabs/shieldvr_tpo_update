﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TPO_Event_Trigger : MonoBehaviour
{
    public TPO_Collider_Controller colliderController;

    // Displaying TPO category arrow UI

    public int TriggerEvent;

    void Start()
    {
        colliderController = transform.GetComponentInParent<TPO_Collider_Controller>();

        if (colliderController == null) Debug.Log("Didn't find a ColliderController on a parent gameObject");

    }

    public void HandleCollisionEntered()
    {
        colliderController.TurnOffAllCollidersExcept(this);
    }


}
