﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductManager : MonoBehaviour
{
    public AreaOnFace[] areasOnFace = new AreaOnFace[5];

    public DialogueManager dialogue;

    public Button[] buttons = new Button[12];

    public void OnActionButton(string name)
    {
        foreach (AreaOnFace area in areasOnFace)
        {
            if (name == area.name)
            {
                Debug.Log("Area selected!");


                foreach (Product pr in area.products)
                {
                    Debug.Log(pr.name);

                    foreach (string strInjects in pr.injectplacements)
                    {
                        Debug.Log(strInjects);

                    }
                }
            }
        }
    }


    void Start()
    {
        areasOnFace[0] = new AreaOnFace("GL_AOF");
        areasOnFace[1] = new AreaOnFace("MF_AOF");
        areasOnFace[2] = new AreaOnFace("NLF_AOF");
        areasOnFace[3] = new AreaOnFace("Lips_AOF");
        areasOnFace[4] = new AreaOnFace("PL_AOF");



        areasOnFace[0].products[0] = new Product("d_product");



        areasOnFace[1].products[0] = new Product("r_product");


        areasOnFace[2].products[0] = new Product("r_product");
        areasOnFace[2].products[1] = new Product("rl_product");
        areasOnFace[2].products[2] = new Product("rd_product");


        areasOnFace[3].products[3] = new Product("rs_product");



        areasOnFace[4].products[4] = new Product("rd_product");
    }

}
