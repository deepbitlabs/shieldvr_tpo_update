﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombinationManager : MonoBehaviour
{
    public CombinationCheck comboCheck;
    public CheckWrongProductCombo productCheck;
    public CheckWrongIPCombo ipCombo;

    // Correct Product Selection
    public CorrectProductCheck correct_product_check;

    // Correct IP Selection
    public CorrectIPCheck correct_ip_check;



    void Start()
    {
        comboCheck = GetComponent<CombinationCheck>();
        productCheck = GetComponent<CheckWrongProductCombo>();
        ipCombo = GetComponent<CheckWrongIPCombo>();


        correct_product_check = GetComponent<CorrectProductCheck>();

        correct_ip_check = GetComponent<CorrectIPCheck>();
    }

    public void ResetEverything()
    {
        comboCheck.ResetButtonSelection();
        productCheck.ResetButtonSelection();
        ipCombo.ResetButtonSelect();
    }


    // Removing gameobject from lists

    public void ResetComboSelect(GameObject myButton)
    {
        comboCheck.ResetSelection(myButton);
    }

}
