﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckWrongProductCombo : MonoBehaviour
{
    public List<GameObject> productcombo1;
    public List<GameObject> productcombo2;
    public List<GameObject> productcombo3;
    public List<GameObject> productcombo4;
    public List<GameObject> productcombo5;
    public List<GameObject> productcombo6;
    public List<GameObject> productcombo7;
    public List<GameObject> productcombo8;
    public List<GameObject> productcombo9;
    public List<GameObject> productcombo10;
    public List<GameObject> productcombo11;
    public List<GameObject> productcombo12;
    public List<GameObject> productcombo13;
    public List<GameObject> productcombo14;
    public List<GameObject> productcombo15;
    public List<GameObject> productcombo16;
    public List<GameObject> productcombo17;
    public List<GameObject> productcombo18;
    List<List<GameObject>> product_combinations;

    public List<GameObject> currentlyActive;


    public DialogueManager dialogue;

    // Reset Everything!
    public CombinationManager comboManager;

    // Correct product check
    public CorrectProductCheck correct_project;

    void Start()
    {

        comboManager = GetComponent<CombinationManager>();

        correct_project = GetComponent<CorrectProductCheck>();


        product_combinations = new List<List<GameObject>>()
        {
            productcombo1,
            productcombo2,
            productcombo3,
            productcombo4,
            productcombo5,
            productcombo6,
            productcombo7,
            productcombo8,
            productcombo9,
            productcombo10,
            productcombo11,
            productcombo12,
            productcombo13,
            productcombo14,
            productcombo15,
            productcombo16,
            productcombo17,
            productcombo18
        };
    }

    public void Product_Button(GameObject button)
    {
        if (!currentlyActive.Contains(button))
        {
            currentlyActive.Add(button);
        }
        else if (currentlyActive.Contains(button))
        {
            currentlyActive.Remove(button);
        }

        bool wrongproduct = false;


        foreach (List<GameObject> product_combination in product_combinations)
        {
            int matches = 0;

            foreach (GameObject chosenButton in currentlyActive)
            {
                if (product_combination.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count == product_combination.Count && matches == product_combination.Count)
            {
                wrongproduct = true;
                Debug.Log("Wrong product selected!");

                dialogue.dialogue.text = dialogue.dialogue25;

                //matches--;

                // Removing gameobject from currently active list
                comboManager.ResetComboSelect(button);



                currentlyActive.Remove(button);

                correct_project.currentlyActive.Remove(button);


                //comboManager.ResetEverything();
            }
        }
    }


    public void ResetButtonSelection()
    {
        currentlyActive.Clear();

    }
}
