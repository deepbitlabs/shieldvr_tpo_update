﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductCombination : MonoBehaviour
{
    public List<GameObject> productCombo1;
    public List<GameObject> productCombo2;
    public List<GameObject> productCombo3;
    public List<GameObject> productCombo4;
    public List<GameObject> productCombo5;
    public List<GameObject> productCombo6;
    public List<GameObject> productCombo7;
    public List<GameObject> productCombo8;
    List<List<GameObject>> productCombinations;

    public List<GameObject> currentlyActive;

    public DialogueManager dialogue;

    void Start()
    {
        productCombinations = new List<List<GameObject>>()
        {
            productCombo1,
            productCombo2,
            productCombo3,
            productCombo4,
            productCombo5,
            productCombo6,
            productCombo7,
            productCombo8
        };
    }

    public void ButtonPressed(GameObject button)
    {
        if (!currentlyActive.Contains(button))
        {
            currentlyActive.Add(button);
        }
        else if (currentlyActive.Contains(button))
        {
            currentlyActive.Remove(button);
        }

        bool WrongProduct = false;

        foreach (List<GameObject> productCombinations in productCombinations)
        {
            int matches = 0;

            foreach (GameObject chosenButton in currentlyActive)
            {
                if (productCombinations.Contains(chosenButton))
                {
                    matches++;
                }
            }

            if (currentlyActive.Count > productCombinations.Count && matches != productCombinations.Count)
            {
                Debug.Log("Wrong product selected!");
                WrongProduct = true;
                dialogue.dialogue.text = dialogue.dialogue25;
            }
        }
    }
}
