﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TPO_Answer_Check : MonoBehaviour
{
    public Button[] tpo_aof = new Button[5];
    public Button[] tpo_product = new Button[5];
    public Button[] tpo_ip = new Button[5];

    public DialogueManager textManager;

    public void OnButtonAction()
    {
        foreach (Button aof in tpo_aof)
        {
            Debug.Log("Selected" + aof);
            
        }
    }

    public void OnButtonAction2()
    {
        foreach (Button aof in tpo_aof)
        {
            Debug.Log("Testing2");
        }
    }

    public void OnButtonAction3()
    {
        foreach (Button aof in tpo_aof)
        {
            Debug.Log("Testing3");
            textManager.dialogue.text = textManager.dialogue23;
        }
    }

    public void CheckForCorrectAnswers()
    {
        if (tpo_aof[0] == tpo_product[0] == tpo_ip[0])
        {
            Debug.Log("Testing if correct answers");
        }
    }
}
