﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;

public class InjectionHaptics : MonoBehaviour {

    public GameObject DysportNiddle;
    public GameObject RestylaneNiddle;

    public DialogueManager injectionText;

    public GameManager gameManager;

	void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Left" || col.gameObject.tag == "Right")
        {
            if (gameObject.tag == "dysport_niddle")
            {
                Debug.Log("Grabbing object now!");
                //gameManager.DysportInjuctionMessagePrompt.GetComponent<Text>().enabled = true;

                injectionText.dialogue.text = injectionText.dialogue11;


                // Displaying Injection Controls
                gameManager.injectionDisplay.SetActive(true);

                // Disabling Graph Interaction Controls
                gameManager.graphDisplay.SetActive(false);
            }

            if (gameObject.tag == "restylane_niddle")
            {
                Debug.Log("Grabbing object now!");

                //gameManager.DysportInjuctionMessagePrompt.GetComponent<Text>().enabled = false;

                //gameManager.RystylanetInjuctionMessagePrompt.GetComponent<Text>().enabled = true;
                injectionText.dialogue.text = injectionText.dialogue12;

                // Displaying Injection Controls
                gameManager.injectionDisplay.SetActive(true);

                // Disabling Graph Interaction Controls
                gameManager.graphDisplay.SetActive(false);

            }
        }
        
    }
}
