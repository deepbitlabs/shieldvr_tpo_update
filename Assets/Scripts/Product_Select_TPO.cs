﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Product_Select_TPO : TPO_UI {

    public GameObject[] GL_Injection;

    public GameObject[] MF_Injection;

    public GameObject[] NLF_Injection;

    public GameObject[] Lip_Injection;

    public GameObject[] PL_Injection;


    public GameObject dialogue;

    public int triggerevent;


    void Awake()
    {
        DialogueManager dialogue = GetComponent<DialogueManager>();
        MeshRenderer enableMesh = GetComponent<MeshRenderer>();

        triggerevent = 0;
    }



    public void EnableGL()
    {
        if (triggerevent == 1)
        {
            foreach (GameObject enableGL in GL_Injection)
            {
                enableGL.GetComponent<MeshRenderer>().enabled = true;
                //Enable_GL_Dysport_UI();

            }
        }
    }

    public void EnableMF()
    {
        if (triggerevent == 2)
        {
            foreach (GameObject enableGL in GL_Injection)
            {
                enableGL.GetComponent<MeshRenderer>().enabled = true;
                //Enable_GL_Dysport_UI();

            }
        }
    }

    public void EnableNLF()
    {
        foreach (GameObject enableGL in GL_Injection)
        {
            enableGL.GetComponent<MeshRenderer>().enabled = true;
            //Enable_GL_Dysport_UI();

        }
    }

    public void EnableLips()
    {
        foreach (GameObject enableGL in GL_Injection)
        {
            enableGL.GetComponent<MeshRenderer>().enabled = true;
            //Enable_GL_Dysport_UI();

        }
    }

    public void EnablePL()
    {
        foreach (GameObject enableGL in GL_Injection)
        {
            enableGL.GetComponent<MeshRenderer>().enabled = true;
            //Enable_GL_Dysport_UI();
            dialogue.GetComponent<DialogueManager>().EndTreatmentPlan();
        }
    }
}
