﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class FacialCollision : MonoBehaviour
{

    // GameManager
    public GameManager gameManager;

    public MeshRenderer renderer;

    // Haptic feedback script
    public HapticEvents haptics;

    // Calling Dialogue Script
    public DialogueManager dialogueText;

    public InjectionCounter dysportCounter;

    public RestylaneCounter restylaneCounter;

    public int restylaneCounterValue = 1;

    public int dysportCounterValue = 1;

    public AudioSource audio;

    FacePoint facePoint;

    public GameObject[] dysportanswers;

    public GameObject[] restylane_answers;


    void Start()
    {
        facePoint = GetComponent<FacePoint>();
        if(facePoint == null)
        {
            Debug.Log("There is no FacePoint script on this object named " + this.gameObject.name);
        }
        renderer.enabled = false;
        //audio.enabled = false;
    }

    public void EnableDysportAnswers()
    {
        foreach(GameObject dysportanswers in dysportanswers)
        {
            dysportanswers.GetComponent<MeshRenderer>().enabled = true;
        }
    }

    public void EnableRestylaneAnswers()
    {
        foreach (GameObject restylane_answers in restylane_answers)
        {
            restylane_answers.GetComponent<MeshRenderer>().enabled = true;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        //if (col.gameObject.name.Contains("Point")) return;

        Debug.Log("Facepoint is colliding with " + col.gameObject.name);
        //facePoint.HandleFacePointHit();



        if (col.gameObject.tag == "restylane_selector")
        {
            if (gameObject.tag == "TriggerPoint1" || gameObject.tag == "TriggerPoint2")
            {

                restylaneCounterValue = facePoint.GetNumberOfPointsAlreadyAdded();

                if (restylaneCounterValue <= 2)
                {

                    facePoint.HandleFacePointHit();
                    // Other Functions

                    //Triggering point 1 for UI actions
                    Debug.Log(col.gameObject.tag + " Has collided with " + gameObject.tag);

                    // Enabling Injection Prompt for Restylane

                    dialogueText.dialogue.text = dialogueText.dialogue12;


                    // Enabling Mesh Renderer
                    renderer.enabled = true;

                    //if (haptics.device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
                    //{
                    //    haptics.OnlyVibrate();
                    //}

                    RestylaneCounter.counter += restylaneCounterValue;

                    haptics.OnlyVibrate();

                    audio.enabled = true;

                    audio.Play();
                }
                if (restylaneCounterValue == 2)
                {
                    //Physics.IgnoreCollision

                    renderer.enabled = false;

                    audio.enabled = false;

                    GetComponent<SphereCollider>().enabled = false;

                    EnableRestylaneAnswers();
                }
            }
        }


        if (col.gameObject.tag == "dysport_selector")
        {
            if (gameObject.tag == "TriggerPoint3" || gameObject.tag == "TriggerPoint4" || gameObject.tag == "TriggerPoint5" || gameObject.tag == "TriggerPoint6" || gameObject.tag == "TriggerPoint7" || gameObject.tag == "TriggerPoint8" || gameObject.tag == "TriggerPoint9" || gameObject.tag == "TriggerPoint10" || gameObject.tag == "TriggerPoint11" )
            {
                restylaneCounterValue = facePoint.GetNumberOfPointsAlreadyAdded();

                if (dysportCounterValue <= 11)
                {

                    facePoint.HandleFacePointHit();

                    //Triggering point 1 for UI actions
                    Debug.Log(col.gameObject.tag + " Has collided with " + gameObject.tag);

                    // Enabling Injection Prompt for Restylane
                    dialogueText.dialogue.text = dialogueText.dialogue11;

                    // Enabling Mesh Renderer
                    renderer.enabled = true;

                    InjectionCounter.counter += dysportCounterValue;

                    // Haptic feedback
                    haptics.OnlyVibrate();

                    audio.enabled = true;

                    audio.Play();
                }

                if (dysportCounterValue == 11)
                {
                    renderer.enabled = false;


                    GetComponent<SphereCollider>().enabled = false;

                    audio.enabled = false;

                    EnableDysportAnswers();
                }

            }
        }
    }
}