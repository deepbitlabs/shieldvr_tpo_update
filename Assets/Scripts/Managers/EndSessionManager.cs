﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSessionManager : MonoBehaviour
{
    public DialogueManager messageQue;

    
    

    public void RevealAnswers()
    {
        messageQue.dialogue.text = messageQue.dialogue16;

        EnableAnswers();


    }

    public GameObject[] fa_spots;
    public GameObject[] injection_point_visuals;

    public void EnableAnswers()
    {
        foreach (GameObject fa_spots in fa_spots)
        {
            fa_spots.GetComponent<MeshRenderer>().enabled = true;
        }

        foreach (GameObject injection_point_visuals in injection_point_visuals)
        {
            injection_point_visuals.SetActive(true);
        }
    }
}
