﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class HapticEvents : MonoBehaviour 
{
	public bool hapticFlag = false;
	public SteamVR_TrackedObject trackedObject;
	public SteamVR_Controller.Device device;

    public SteamVR_LaserPointer laser;

    //public GameObject graphSelector;
    //public GameObject restylane_selector;
    //public GameObject dysport_selector;

    public GameObject laser_pointer;

    void Start()
	{
		trackedObject = GetComponent<SteamVR_TrackedObject> ();
		device = SteamVR_Controller.Input ((int)trackedObject.index);


        // Only Selected Objects
        //graphSelector.SetActive(false);
        //restylane_selector.SetActive(false);
        //dysport_selector.SetActive(false);
        

    }

	void Update()
	{
		device = SteamVR_Controller.Input ((int)trackedObject.index);

        if (device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
        {
            //restylane_selector.SetActive(true);
            //dysport_selector.SetActive(true);
            //graphSelector.SetActive(true);
            //laser.enabled = false;
        }

        if (device.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad))
        {
            //restylane_selector.SetActive(false);
            //dysport_selector.SetActive(false);
            //graphSelector.SetActive(false);
        }
    }


    public void OnlyVibrate()
    {
        device.TriggerHapticPulse(500);


        //if (hapticFlag)
        //{
        //    device.TriggerHapticPulse(500);
        //}
    }
}
