﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FA_Correct_Answers : MonoBehaviour
{
    public GameObject[] fa_spots;

    public void EnableAnswers()
    {
        foreach (GameObject fa_spots in fa_spots)
        {
            fa_spots.GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
