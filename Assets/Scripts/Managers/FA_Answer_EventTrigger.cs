﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FA_Answer_EventTrigger : MonoBehaviour
{
    public FA_Answer_UI fa_UI;

    public void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "graphSelector")
        {
            // Symmetry
            if (gameObject.tag == "0_Symmetry")
            {
                fa_UI.dialogue.text = fa_UI.dialogue0;

            }

            if (gameObject.tag == "1_Symmetry")
            {
                fa_UI.dialogue.text = fa_UI.dialogue1;

            }

            if (gameObject.tag == "2_Symmetry")
            {
                fa_UI.dialogue.text = fa_UI.dialogue2;

            }

            if (gameObject.tag == "3_Symmetry")
            {
                fa_UI.dialogue.text = fa_UI.dialogue3;

            }

            // Proportions
            if (gameObject.tag == "0_proportions")
            {
                fa_UI.dialogue.text = fa_UI.dialogue0;

            }

            if (gameObject.tag == "1_proportions")
            {
                fa_UI.dialogue.text = fa_UI.dialogue1;

            }

            if (gameObject.tag == "2_proportions")
            {
                fa_UI.dialogue.text = fa_UI.dialogue2;

            }

            if (gameObject.tag == "3_proportions")
            {
                fa_UI.dialogue.text = fa_UI.dialogue3;

            }


            // Facial Shape Volume Loss
            if (gameObject.tag == "0_fs_vl")
            {
                fa_UI.dialogue.text = fa_UI.dialogue0;

            }

            if (gameObject.tag == "1_fs_vl")
            {
                fa_UI.dialogue.text = fa_UI.dialogue1;

            }

            if (gameObject.tag == "2_fs_vl")
            {
                fa_UI.dialogue.text = fa_UI.dialogue2;

            }

            if (gameObject.tag == "3_fs_vl")
            {
                fa_UI.dialogue.text = fa_UI.dialogue3;

            }

            // Facial Shape Sagging
            if (gameObject.tag == "0_fs_sagging")
            {
                fa_UI.dialogue.text = fa_UI.dialogue0;

            }

            if (gameObject.tag == "1_fs_sagging")
            {
                fa_UI.dialogue.text = fa_UI.dialogue1;

            }

            if (gameObject.tag == "2_fs_sagging")
            {
                fa_UI.dialogue.text = fa_UI.dialogue2;

            }

            if (gameObject.tag == "3_fs_sagging")
            {
                fa_UI.dialogue.text = fa_UI.dialogue3;

            }

            // Skin Quality Loss Of Firmness
            if (gameObject.tag == "0_sq_LOF")
            {
                fa_UI.dialogue.text = fa_UI.dialogue0;

            }

            if (gameObject.tag == "1_sq_LOF")
            {
                fa_UI.dialogue.text = fa_UI.dialogue1;

            }

            if (gameObject.tag == "2_sq_LOF")
            {
                fa_UI.dialogue.text = fa_UI.dialogue2;

            }

            if (gameObject.tag == "3_sq_LOF")
            {
                fa_UI.dialogue.text = fa_UI.dialogue3;

            }

            // Skin Quality LOR/Glow
            if (gameObject.tag == "0_sq_LOF_Glow")
            {
                fa_UI.dialogue.text = fa_UI.dialogue0;

            }

            if (gameObject.tag == "1_sq_LOF_Glow")
            {
                fa_UI.dialogue.text = fa_UI.dialogue1;

            }

            if (gameObject.tag == "2_sq_LOF_Glow")
            {
                fa_UI.dialogue.text = fa_UI.dialogue2;

            }

            if (gameObject.tag == "3_sq_LOF_Glow")
            {
                fa_UI.dialogue.text = fa_UI.dialogue3;

            }



            // Expression Dynamic Lines
            if (gameObject.tag == "0_expression_DL")
            {
                fa_UI.dialogue.text = fa_UI.dialogue0;

            }

            if (gameObject.tag == "1_expression_DL")
            {
                fa_UI.dialogue.text = fa_UI.dialogue1;

            }

            if (gameObject.tag == "2_expression_DL")
            {
                fa_UI.dialogue.text = fa_UI.dialogue2;

            }

            if (gameObject.tag == "3_expression_DL")
            {
                fa_UI.dialogue.text = fa_UI.dialogue3;

            }

            // Expression Static Lines
            if (gameObject.tag == "0_expression_SL")
            {
                fa_UI.dialogue.text = fa_UI.dialogue0;

            }

            if (gameObject.tag == "1_expression_SL")
            {
                fa_UI.dialogue.text = fa_UI.dialogue1;

            }

            if (gameObject.tag == "2_expression_SL")
            {
                fa_UI.dialogue.text = fa_UI.dialogue2;

            }

            if (gameObject.tag == "3_expression_SL")
            {
                fa_UI.dialogue.text = fa_UI.dialogue3;

            }
        }
    }
}
