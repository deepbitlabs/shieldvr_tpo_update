﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggeredEvents : MonoBehaviour
{

    public GameManager gameManager;

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Right" || col.gameObject.tag == "Left")
        {
            if (gameObject.tag == "GraphChart")
            {
                gameManager.productCanvas.SetActive(true);
                gameManager.Product1.SetActive(true);
                gameManager.Product2.SetActive(true);
                //gameManager.niddle.SetActive(true);
            }
        }
    }
}
