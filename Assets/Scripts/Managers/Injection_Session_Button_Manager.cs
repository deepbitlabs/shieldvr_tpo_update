﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Injection_Session_Button_Manager : MonoBehaviour
{
    public DialogueManager messageQue;

    //public FA_Correct_Answers fa_spots;

    public GameObject Dysport_Correct_Spots;
    public GameObject Restylane_Correct_Spots;
    public GameObject[] Facial_Visuals;

    public GameObject[] Reset_TPO_Canvas;

    TPO_Event_Trigger tpo_event_trigger;


    // Reset Everything!
    public CombinationCheck comboCheck;

    public void RestartSession(GameObject button)
    {
        // Resetting the facial assessment session
        messageQue.dialogue.text = messageQue.dialogue19;


        comboCheck.ResetButtonSelection();





        DisableAnswers();
    }

    void Start()
    {
        tpo_event_trigger = GetComponent<TPO_Event_Trigger>();

    }


    public GameObject[] fa_spots;

    public void EnableAnswers()
    {
        foreach (GameObject fa_spots in fa_spots)
        {
            fa_spots.GetComponent<MeshRenderer>().enabled = true;
            Enable_Facial_Visuals();
        }
    }

    public void DisableTPO()
    {
        foreach (GameObject tpo_answers in Reset_TPO_Canvas)
        {
            //tpo_answers.SetActive(false);
            tpo_answers.GetComponent<Image>().enabled = false;
        }
    }

    public void DisableAnswers()
    {
        foreach (GameObject fa_spots in fa_spots)
        {
            fa_spots.GetComponent<MeshRenderer>().enabled = false;
            Disable_Facial_Visuals();
        }
    }

    public void Enable_Facial_Visuals()
    {
        foreach (GameObject fv in Facial_Visuals)
        {
            fv.SetActive(true);
        }
    }

    public void Disable_Facial_Visuals()
    {
        foreach (GameObject fv in Facial_Visuals)
        {
            fv.SetActive(false);
        }
    }

    public void EndSession()
    {
        messageQue.dialogue.text = messageQue.dialogue16;


        EnableAnswers();
    }

    public void Select_Next_Product()
    {
        // Select next product
    }
}
