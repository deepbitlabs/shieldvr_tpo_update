﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // Product Canvas
    public GameObject productCanvas;

    // Product Models
    public GameObject Product1;
    public GameObject Product2;

    // Controller Hint Displays
    public GameObject graphDisplay;
    public GameObject injectionDisplay;

    void Start()
    {
        // Display Hints
        graphDisplay.SetActive(false);
        injectionDisplay.SetActive(false);
    }

}
