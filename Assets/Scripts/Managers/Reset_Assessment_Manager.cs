﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset_Assessment_Manager : MonoBehaviour
{
    public InjectionCounter dysport;
    public InjectionCounter restylane;

    public DialogueManager dialm;

    public FacialCollision[] facial;

    

    public void ResetCounter()
    {
        dysport.Reset();
        restylane.Reset();
        EnableAnswers();
        //message_box();

        //foreach (FacialCollision facial in facial)
        //{
        //    facial.renderer.enabled = false;
        //}
    }

    public GameObject[] fa_spots;

    public void EnableAnswers()
    {
        foreach (GameObject fa_spots in fa_spots)
        {
            fa_spots.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    public void message_box()
    {
        dialm.dialogue.text = dialm.dialogue18;
    }
}
