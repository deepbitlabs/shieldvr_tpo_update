﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductSelection : MonoBehaviour
{
    // Selection Messages
    public Text SuccessMessage;
    public Text ErrorMessage;

    // Products
    public GameObject Dysport;
    public GameObject Restylane;

    void Start()
    {
        //SuccessMessage.GetComponent<Text>().enabled = false;
        //ErrorMessage.GetComponent<Text>().enabled = false;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Niddle" || col.gameObject.tag == "Right" || col.gameObject.tag == "Left")
        {
            if (gameObject.tag == "Product1")
            {
                SuccessMessage.GetComponent<Text>().enabled = false;
            }
            else
            {
                return;
            }

            if (gameObject.tag == "Product2")
            {
                ErrorMessage.GetComponent<Text>().enabled = false;
            }
            else
            {
                return;
            }
        }
    }


}
