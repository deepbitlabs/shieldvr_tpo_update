﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacialGroupManager : MonoBehaviour {

    public FacePoint[] facepoints;

    public InjectionCounter injectionCounter;

    public FacialCollision[] facial_collision;




    public void AccessMesh(FacialCollision face)
    {
        foreach (FacialCollision facial in facial_collision)
        {

            
            MeshRenderer ren = facial.GetComponent<MeshRenderer>();
            //Renderer ren = facial.GetComponent<Renderer>();

            

            if (ren != face)
            {
                ren.enabled = false;
            }
            else
            {
                ren.enabled = true;
            }

        }
    }

    public void AddFacePoint(FacePoint facePoint)
    {
        if(injectionCounter.activeFacePoints.Count < injectionCounter.maxNumberOfFacePoints)
        {
            injectionCounter.activeFacePoints.Add(facePoint);
            
           
        } else
        {
            Debug.Log("Can't add face point to list, already full");
        }
    }


    public void RemoveFacePoint(FacePoint facePoint)
    {
        if (injectionCounter.activeFacePoints.Contains(facePoint))
        {
            injectionCounter.activeFacePoints.Remove(facePoint);
        } else
        {
            Debug.Log("Can't remove face point, it's not in the list of active face points");
        }
    }

	// Use this for initialization
	public void Init () {
        facepoints = GetComponentsInChildren<FacePoint>();

        facial_collision = transform.GetComponentsInChildren<FacialCollision>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
