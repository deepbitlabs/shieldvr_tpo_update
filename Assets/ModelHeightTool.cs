﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelHeightTool : MonoBehaviour
{
    public GameObject facialModel;
    public float speed = 1.5f;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Left" || col.gameObject.tag == "Right" || col.gameObject.tag == "TestModel")
        {
            if (gameObject.tag == "Raise")
            {
                RaiseModel();

                //facialModel.transform.position = new Vector3(1.272f, -1.282f, 0.045f * speed);
            }

            if (gameObject.tag == "Lower")
            {
                LowerModel();

                //facialModel.transform.position = new Vector3(1.272f, -1.691f, 0.045f);
            }

            if (gameObject.tag == "rotate_left")
            {
                RotateLeft();
            }

            if (gameObject.tag == "rotate_right")
            {
                RotateRight();
            }
        }
    }

    
    public void RotateLeft()
    {
        //facialModel.transform.Rotate(Vector3.forward * speed * Time.deltaTime);

        facialModel.transform.Rotate(new Vector3(Time.deltaTime * 0,10,0));

        Debug.Log("Rotating gameobject left!");
    }

    public void RotateRight()
    {
        //facialModel.transform.Rotate(-Vector3.forward * speed * Time.deltaTime);

        facialModel.transform.Rotate(new Vector3(Time.deltaTime * 0, -10, 0));

        Debug.Log("Rotating gameobject right!");
    }

    public void RaiseModel()
    {
        facialModel.transform.position = new Vector3(1.272f, -1.282f, 0.045f * speed);
    }

    public void LowerModel()
    {
        facialModel.transform.position = new Vector3(1.272f, -1.691f, 0.045f);
    }
}
