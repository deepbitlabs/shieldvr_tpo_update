﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderChild : MonoBehaviour {

    ColliderController colliderController;

    // Use this for initialization
    void Start () {
        colliderController = transform.GetComponentInParent<ColliderController>();

        if (colliderController == null) Debug.Log("Didn't find a ColliderController on parent gameObject");

	}

    private void Update()
    {
        // This is debug test code, remove it if it works

        if (Input.GetKeyDown(KeyCode.Space))
        {
            HandleCollisionEntered();
        }

    }

    void HandleCollisionEntered()
    {
   //     colliderController.TurnOffAllCollidersExcept(this); // This disables all EXCEPT this gameObject
        // 
    }

    void OnCollisionEnter(Collision collision)
    {
        HandleCollisionEntered();
    }


}
