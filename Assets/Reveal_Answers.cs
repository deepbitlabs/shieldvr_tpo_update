﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reveal_Answers : MonoBehaviour
{
    public GameObject[] revealAnswers;

    void Start()
    {
        foreach (GameObject r in revealAnswers)
        {
            r.SetActive(true);
        }
    }
}
