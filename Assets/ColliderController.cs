﻿using UnityEngine;

public class ColliderController : MonoBehaviour {

    [SerializeField]
    GraphEventTrigger[] children;


	// Use this for initialization
	void Start () {
        children = transform.GetComponentsInChildren<GraphEventTrigger>();

        // Debug.Log("Found " + transform.childCount + " children");
    }
	

    public void TurnOffAllCollidersExcept(GraphEventTrigger graphEventTrigger)
    {

        Debug.Log("TurnOffAllCollidersExcept called by " + graphEventTrigger.gameObject.name);

        foreach (GraphEventTrigger child in children)
        {

            Renderer ren = child.GetComponent<Renderer>();
            if (child != graphEventTrigger)
            {
                
                ren.enabled = false;
                Debug.Log("Disabled collider on: " + child.name);
                
            } else
            {
                ren.enabled = true;
                Debug.Log("Enabled collider on: " + child.name);
            }
        }
    }
}
