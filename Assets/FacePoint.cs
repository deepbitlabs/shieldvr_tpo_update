﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacePoint : MonoBehaviour {

    FacialGroupManager manager;




    public int GetNumberOfPointsAlreadyAdded()
    {
        return manager.injectionCounter.activeFacePoints.Count;
    }

    public void HandleFacePointHit()
    {
        manager.AddFacePoint(this);        
    }

	// Use this for initialization
	void Start () {
        manager = transform.GetComponentInParent<FacialGroupManager>();
        if(manager == null)
        {
            Debug.Log("Manager is missing on " + gameObject.name);
        }


	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
